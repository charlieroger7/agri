<?php

namespace App\Controller;

use App\Entity\Reservation;
use App\Form\ReservationType;
use App\Repository\VisitesRepository;
use App\Repository\ReservationRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ReservationController extends AbstractController
{
    /**
     * @Route("/reservation/{id}", name="reservation")
     */
    public function reservation($id, VisitesRepository $visitesRepository, ReservationRepository $reservationRepository, Request $request): Response
    {
        if ($this->getUser()) {
            $visite = $visitesRepository->findOneBy(array('id' => $id));

            //$dateNow = new \DateTime('NOW');
            //$dateStart = $visite->getDateStart();

            //if ($dateStart->$dateNow){
            $isReservation = $reservationRepository->findOneBy(array("user" => $this->getUser()->getId(), "visites" => $id)) ? true
                : false;

            if (!$isReservation) {

                $reservation = new Reservation();
                $formReservation = $this->createForm(ReservationType::class, $reservation);
                $formReservation->handleRequest($request);

                if ($formReservation->isSubmitted() && $formReservation->isValid()) {
                    $entityManager = $this->getDoctrine()->getManager();

                    $reservation->setUser($this->getUser());
                    $reservation->setVisites($visite);

                    $reservation->setDateCreated(new \DateTime());
                    $entityManager->persist($reservation);

                    $entityManager->flush();

                    $this->addFlash('success', 'La réservation a été ajouté avec succés.');
                    return $this->redirectToRoute('homepage');
                }

                return $this->render('reservation/reservation.html.twig', [
                    'formReservation' => $formReservation->createView()
                ]);
            } else {
                $this->addFlash('danger', 'attention, vous avez déjà réservé cette visite');
                return $this->redirectToRoute('homepage');
            }


            //} else{
            //    $this->addFlash('danger', 'attention, la date de début est dépassé');
            //return $this->redirectToRoute('homepage');
            //}


        } else {
            $this->addFlash('danger', 'attention, vous devez vous connécter pour réserver une visite');
            return $this->redirectToRoute('homepage');
        }

        return $this->render('reservation/index.html.twig', [
            'controller_name' => 'ReservationController',
        ]);
    }
}
