<?php

namespace App\Controller\Admin;

use App\Entity\Visites;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class VisitesCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Visites::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
