<?php

namespace App\Controller;

use App\Entity\Visites;
use App\Form\VisiteType;
use App\Repository\CategoryRepository;
use App\Repository\VisitesRepository;
use Doctrine\ORM\EntityManagerInterface;
use PhpParser\Node\Stmt\Return_;
use Proxies\__CG__\App\Entity\Category;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\String\Slugger\SluggerInterface;

class VisiteController extends AbstractController
{


    /**
     * @Route("/{slug}", name="visite_category", priority=-1)
     */
    public function category($slug, CategoryRepository $categoryRepository): Response
    {
        $category = $categoryRepository->findOneBy([
            'slug' => $slug,
        ]);

        if (!$category) {
            throw $this->createNotFoundException("La catégorie demandé n'existe pas");
        }

        return $this->render('visite/category.html.twig', [
            'slug' => $slug,
            'category' => $category
        ]);
    }

    /**
     * @Route("/{category_slug}/{slug}", name="visite_show")
     * @Route("/{slug}", name="visite_show")
     */
    public function show($slug, VisitesRepository $visitesRepository)
    {
        $visite = $visitesRepository->findOneBy([
            'slug' => $slug
        ]);

        if (!$visite) {
            throw $this->createNotFoundException("La visite demandée n'existe pas");
        }


        return $this->render('visite/show.html.twig', [
            'visite' => $visite
        ]);
    }



    /**
     * @Route("/admin/visite/{id}/edit", name="visite_edit")
     */
    public function edit($id, VisitesRepository $visitesRepository, Request $request, EntityManagerInterface $em, UrlGeneratorInterface $urlGenerator)
    {
        $visite = $visitesRepository->find($id);

        $form = $this->createForm(VisiteType::class, $visite);

        // $form->setData($visite);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $em->flush();


            // $response = new RedirectResponse($url);

            // return $response;

            return $this->redirectToRoute('visite_show', [
                'category_slug' => $visite->getCategory()->getSlug(),
                'slug' => $visite->getSlug()
            ]);
        }


        $formView = $form->createView();

        return $this->render('visite/edit.html.twig', [
            'visite' => $visite,
            'formView' => $formView
        ]);
    }

    /**
     * @Route("/admin/visite/create", name="visite_create")
     */
    public function create(FormFactoryInterface $factory, Request $request, SluggerInterface $slugger, EntityManagerInterface $em)
    {
        $visite = new Visites;

        $form = $builder = $this->createForm(VisiteType::class, $visite);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $visite->setSlug(strtolower($slugger->slug($visite->getName())));

            $em->persist($visite);
            $em->flush();

            return $this->redirectToRoute('visite_show', [
                'category_slug' => $visite->getCategory()->getSlug(),
                'slug' => $visite->getSlug()
            ]);
        }



        $formView = $form->createView();

        return $this->render('visite/create.html.twig', [
            'formView' => $formView
        ]);
    }
}
