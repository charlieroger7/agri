<?php

namespace App\Controller;

use App\Repository\VisitesRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{

    /**
     * @Route("/", name="homepage")
     */

    public function homepage(VisitesRepository $visitesRepository)
    {

        $visites = $visitesRepository->findBy([], [], 3);

        return $this->render('home.html.twig', [
            'visites' => $visites
        ]);
    }
}
