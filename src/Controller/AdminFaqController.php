<?php

namespace App\Controller;

use App\Entity\Faq;
use App\Form\FaqType;
use App\Repository\FaqRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/faq")
 */
class AdminFaqController extends AbstractController
{
    /**
     * @Route("/", name="faq_index", methods={"GET","POST"})
     */
    public function index(Request $request, FaqRepository $faqRepository): Response
    {
        $faq = new Faq();
        $form = $this->createForm(FaqType::class, $faq);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $faq->setDateUpdated(new \DateTime());

            $entityManager->persist($faq);
            $entityManager->flush();

            $this->addFlash('success', 'La question a été ajoutée avec succès.');
            return $this->redirectToRoute('faq_index');
        }

        return $this->render('admin/faq/index.html.twig', [
            'active' => 'active_faq',

            'faqs' => $faqRepository->findAll(),
            'form' => $form->createView()
        ]);
    }


    /**
     * @Route("/{id}/edition", name="faq_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Faq $faq): Response
    {
        $form = $this->createForm(FaqType::class, $faq);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $faq->setDateUpdated(new \DateTime());

            $entityManager->persist($faq);
            $entityManager->flush();

            $this->addFlash("info", "La question a bien été mise à jour.");
            return $this->redirectToRoute('faq_index');
        }

        return $this->render('admin/faq/edit.html.twig', [
            'active' => 'active_edit_faq',

            'faq'  => $faq,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="faq_delete", methods={"POST"})
     */
    public function delete(Request $request, Faq $faq): Response
    {
        if ($this->isCsrfTokenValid('delete' . $faq->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($faq);
            $entityManager->flush();
        }

        $this->addFlash('success', 'La question a été supprimée avec succès.');
        return $this->redirectToRoute('faq_index');
    }
}
