<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Entity\Faq;
use App\Form\ContactType;
use App\Repository\FaqRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class MainController extends AbstractController
{

    //ici la route pour accéder au formulaire de contact

    /**
     * @Route("/contact", name="contact_index")
     */
    public function contact(Request $request): Response
    {

        $contact = new Contact();
        $formContact = $this->createForm(ContactType::class, $contact);
        $formContact->handleRequest($request);

        if ($formContact->isSubmitted() && $formContact->isValid()) {

            $contact->setIsHandled(false);
            $contact->setDateCreated(new \DateTime());

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($contact);
            $entityManager->flush();

            $this->addFlash('info', 'votre message est parti, merci.');
            return $this->redirectToRoute('contact_index');
        }
        return $this->render('main/contact.html.twig', [
            'formContact' => $formContact->createView()
        ]);
    }

    /**
     * @Route("/faq", name="faq_index")
     */
    public function faq(FaqRepository $faqRepository): Response
    {

        return $this->render('faq/index.html.twig', [
            'faqs' => $faqRepository->findAll(),
        ]);
    }
}
