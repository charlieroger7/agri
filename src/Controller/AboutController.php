<?php

namespace App\Controller;

use App\Repository\AboutRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AboutController extends AbstractController
{
    /**
     * @Route("/about", name="about")
     */
    public function About(AboutRepository $aboutRepository): Response
    {
        $about = $aboutRepository->findAll();

        return $this->render('about/About.html.twig', [
            'controller_name' => 'AboutController',
            'about' => $about
        ]);
    }
}
