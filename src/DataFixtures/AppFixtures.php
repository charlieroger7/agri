<?php

namespace App\DataFixtures;

use App\Entity\About;
use Faker\Factory;
use App\Entity\Faq;
use App\Entity\User;
use App\Entity\Visites;
use App\Entity\Category;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    protected $slugger;
    protected $encoder;

    public function __construct(SluggerInterface $slugger, UserPasswordEncoderInterface $encoder)
    {
        $this->slugger = $slugger;
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');
        $faker->addProvider(new \Liior\Faker\Prices($faker));
        $faker->addProvider(new \Bezhanov\Faker\Provider\Commerce($faker));
        $faker->addProvider(new \Bluemmb\Faker\PicsumPhotosProvider($faker));


        $admin = new User;

        $hash = $this->encoder->encodePassword($admin, "password");

        $admin->setEmail("admin@gmail.com")
            ->setPassword($hash)
            ->setFirstName("Charlie")
            ->setName("Charlie")
            ->setRoles(['ROLE_ADMIN']);

        $manager->persist($admin);

        for ($u = 0; $u < 5; $u++) {
            $user = new User();


            $hash = $this->encoder->encodePassword($admin, "password");


            $user->setEmail("user$u@gmail.com")
                ->setFirstName($faker->name())
                ->setName($faker->name())
                ->setPassword($hash);

            $manager->persist($user);
        }

        for ($c = 0; $c < 3; $c++) {
            $category = new Category;
            $category->setName($faker->department)
                ->setSlug(strtolower($this->slugger->slug($category->getName())));

            $manager->persist($category);

            for ($p = 0; $p < mt_rand(15, 20); $p++) {
                $visites = new Visites;
                $visites->setName($faker->productName)
                    ->setPrice($faker->price(2000, 8000))
                    ->setSlug($this->slugger->slug($visites->getName()))
                    ->setCategory($category)
                    ->setShortDescription($faker->paragraph())
                    ->setMainPicture($faker->imageUrl(400, 400, true));

                $manager->persist($visites);
            }
        }


        $faq1 = new Faq();
        $faq1->setQuestion('Je n’ai jamais pratiqué de sport en club, est-ce un problème ?')
            ->setAnswer('<p>Agri-visite vise un large public, que vous soyez exp&eacute;riment&eacute; ou non, nous vous accueillons avec grand plaisir.<br />Toute l&#39;&eacute;quipe est l&agrave; pour r&eacute;pondre &agrave; la moindre de vos questions, vous conseiller si vous le souhaitez et vous expliquer comment fonctionnent les machines, les cours collectifs, etc ...<br /><strong><em>Vous ne serez pas le premier ni le dernier &agrave; n&#39;avoir jamais pratiqu&eacute; du sport en salle.</em></strong></p>')
            ->setDateUpdated(new \DateTime());
        $manager->persist($faq1);

        $about = new About();
        $about->setTitre('AGRI-VISITE')
            ->setSousTitre('Le site de la découverte de la nature et de l agriculture 
            Echangez avec les agriculteurs de votre régions')
            ->setTitre2('PARLONS DE AGRI-VISITE')
            ->setTexte1('Agri-visite vous permet de visiter des fermes en famille, entre amis ou dans un contexte pédagogique, dans votre régions. Grace à notre tableau de réservation en ligne disponible sur chaque page d exploitation à visiter, vous pouvez réserver une demi journée pour visiter une exploitation agricole

            Notre site permet aussi de pouvoir échanger avec les agriculteurs et cela de plusieurs manières. D une part grace aux liens qui mène à leurs réseaux sociaux et d une autre part directement sur notre site, en se rendant sur la page F.A.Q.')
            ->setTexte2('Les agriculteurs présent sur notre site sont ravis de pouvoir vous rencontrez et d échanger avec vous. Ils proposent par ailleurs pendant les visites des ventes de produits locaux produit par leurs soins. Cette actions permet de donner un petit coup de pouce au agriculteurs de votre régions et de manger locale.');

        $manager->persist($about);

        $manager->flush();
    }
}
