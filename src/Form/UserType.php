<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, [
                'required' => true,
                'label'    => "E-Mail",
                'attr'     => [
                    'placeholder' => "exemple@agri-visite.fr",
                    'class'       => ''
                ]
            ])


            ->add('firstName', TextType::class, [
                'required' => true,
                'label'    => "Prénom",
                'attr'     => [
                    'placeholder' => "Fabien",
                    'class'       => ''
                ]
            ])

            ->add('name', TextType::class, [
                'required' => true,
                'label'    => "nom",
                'attr'     => [
                    'placeholder' => "roger",
                    'class'       => ''
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
