<?php

namespace App\Form;

use App\Entity\Reservation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class ReservationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('hasValidCondition', CheckboxType::class, [
                'required' => true,
                'label' => "cochez la case des cgu",
                'attr' => [
                    'class' => ''
                ]
            ])
            ->add('email', EmailType::class, [
                'required' => true,
                'label'    => "Adresse email",
                'attr'     => [
                    'placeholder' => "fdegrasse@agri-visite.fr",
                    'class'       => ''
                ]
            ])
            ->add('phone', TelType::class, [
                'required' => true,
                'label'    => "Téléphone",
                'attr'     => [
                    'placeholder' => "0123456789",
                    'class'       => '',
                    'pattern'     => '[0-9]{10}'
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Reservation::class,
        ]);
    }
}
