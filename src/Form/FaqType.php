<?php

namespace App\Form;

use App\Entity\Faq;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class FaqType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('question', TextType::class, [
                'required' => true,
                'label'    => "Question",
                'attr'     => [
                    'placeholder' => "Entrez l'intitulé de la question",
                    'class'       => 'col-sm-12 mb75'
                ]
            ])
            ->add('answer', TextareaType::class, [
                'required' => true,
                'label'    => "Réponse",
                'attr'     => [
                    'placeholder' => "Entrez la réponse de la question",
                    'class'       => 'col-sm-12 mb75',
                    'rows'        => '5'
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Faq::class,
        ]);
    }
}
